import sys
from PyQt6.QtCore import QSize, Qt
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QWidget, QVBoxLayout

class SecondWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("")
        self.setGeometry(100, 100, 200, 200)
        layout = QVBoxLayout()
        label = QLabel("Schedule an appointment!", self)
        layout.addWidget(label)
        self.setLayout(layout)

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Perfekte Designerin")
        self.setStyleSheet("background-color: yellow;")

        # Window Size
        self.setFixedSize(QSize(400, 300))  # oder .setMinimumSize
        self.button = QPushButton("Press Me!?", self)
        self.button.setStyleSheet("background-color:red;")
        self.button.setGeometry(50, 50, 200, 50)
        self.button.clicked.connect(self.the_button_was_clicked)
        self.setCentralWidget(self.button)

    def the_button_was_clicked(self):
        # print("Welcome in the Perfekt Designerin App")
        self.button.setText("You have already clicked me!")
        self.button.setEnabled(False)
        self.setWindowTitle("My Oneshot App")

        self.second_window = SecondWindow()
        self.second_window.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()  # Qt Widget anlegen
    window.show()  # widget anblenden
    sys.exit(app.exec())
