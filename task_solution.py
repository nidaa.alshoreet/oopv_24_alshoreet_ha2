import sys
from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QWidget, QVBoxLayout, QLineEdit, QSpinBox


class ThirdWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("One move to get an appointment!")
        self.setStyleSheet("background-color: White;")
        self.setFixedSize(QSize(400, 300))

        main_layout = QVBoxLayout()
        self.central_widget = QWidget()
        self.central_widget.setLayout(main_layout)
        self.setCentralWidget(self.central_widget)

        label = QLabel("Full your Information", self.central_widget)
        label.setStyleSheet("color: black;")
        main_layout.addWidget(label)

        first_name_label = QLabel("First Name:", self.central_widget)
        first_name_label.setStyleSheet("color: black;")
        second_name_label = QLabel("Second Name:", self.central_widget)
        second_name_label.setStyleSheet("color: black;")
        phone_number_label = QLabel("Your phone number:", self.central_widget)
        phone_number_label.setStyleSheet("color: black;")
        self.first_name_edit = QLineEdit()
        self.second_name_edit = QLineEdit()
        self.phone_number_spinbox = QSpinBox()
        self.phone_number_spinbox.setMaximum(999999)
        self.first_name_edit.setStyleSheet("background-color: cyan;")
        self.phone_number_spinbox.setStyleSheet("background-color: cyan;")
        self.second_name_edit.setStyleSheet("background-color: cyan;")

        main_layout.addWidget(first_name_label)
        main_layout.addWidget(self.first_name_edit)
        main_layout.addWidget(second_name_label)
        main_layout.addWidget(self.second_name_edit)
        main_layout.addWidget(phone_number_label)
        main_layout.addWidget(self.phone_number_spinbox)


class SecondWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Schedule an appointment!")
        self.setStyleSheet("background-color: green;")
        self.setFixedSize(QSize(400, 300))
        layout = QVBoxLayout(self)
        label = QLabel("Lets Be the new Perfekte Designer, Click here ---> ", self)
        layout.addWidget(label)
        self.button2 = QPushButton("Click here", self)
        self.button2.setStyleSheet("background-color:green;")
        self.button2.clicked.connect(self.the_button2_was_clicked)
        layout.addWidget(self.button2)

    def the_button2_was_clicked(self):
        self.button2.setText("You have already clicked me Again!")
        self.button2.setEnabled(False)
        self.setWindowTitle("One move to get an appointment!")
        self.setStyleSheet("background-color: pink;")
        self.setFixedSize(QSize(400, 300))
        self.third_window = ThirdWindow()
        self.third_window.show()


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Perfekte Designerin")
        self.setStyleSheet("background-color: yellow;")
        self.setFixedSize(QSize(400, 300))
        self.button = QPushButton("you want to be new Perfekte Designer? click here", self)
        self.button.setStyleSheet("background-color:red;")
        self.button.clicked.connect(self.the_button_was_clicked)
        self.button.setGeometry(50,120,300,30)
        layout = QVBoxLayout(self)
        layout.addWidget(self.button)

    def the_button_was_clicked(self):
        self.button.setText("You have already clicked me!")
        self.button.setEnabled(False)
        self.setWindowTitle("My Oneshot App")
        self.setFixedSize(QSize(400, 300))
        self.second_window = SecondWindow()
        self.second_window.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
