## Kopfstand (Headstand)

It is a solution for a small project from a master's student for the lab section of (OOP) object oriented programing.

# Task
Develop a small PyQt program that contains poor user experience (UX) design decisions. Among other things, you can use bad color combinations, unnecessary widgets, very small font sizes or confusing layouts.

Please be creative with it and play with the events (QEvents). 